package com.eCommerce;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.eCommerce.Pages.*;
import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.ConfigurationReader;
import com.eCommerce.Utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public abstract class TestBase {

    protected static WebDriver driver;
    protected WebDriverWait wait;
    protected String url;
    public static ExtentReports extent;
    protected static ExtentSparkReporter spark;
    protected static ExtentTest extentLogger;
    protected HomePage homePage;
    protected CartPage cartPage;
    protected CustomerPage customerPage;
    protected LoginPage loginPage;
    protected ProductsPage productsPage;

    @BeforeTest
    public void reportTest() {
        //create a report path
        String projectPath = System.getProperty("user.dir");
        String path = projectPath + "/test-output/report.html";
        //initialize the html reporter with the report path
        spark = new ExtentSparkReporter(path);
        extent = new ExtentReports();
        //attach the html report to report object
        extent.attachReporter(spark);
        //title in report
        spark.config().setReportName(ConfigurationReader.get("reportName"));
        ///set environment information
        extent.setSystemInfo("Fashionette AG", "UI Test");
        extent.setSystemInfo("Environment", "Staging");
        extent.setSystemInfo("Browser", ConfigurationReader.get("browser"));
        extent.setSystemInfo("OS", System.getProperty("os.name"));
    }

    @BeforeMethod
    public void setUpTest() {
        url = ConfigurationReader.get("url");
        driver = Driver.get();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.get(url);
        homePage = new HomePage();
        homePage.AcceptAllCookies();
        String expectedTitle = "fashionette | Buy designer handbags, shoes & accessories online";
        String actualTitle = Driver.get().getTitle();
        assertEquals(expectedTitle, actualTitle);
    }

    // ITestResult class describes the result of a test in TestNG
    @AfterMethod
    public void afterMethod(ITestResult result) throws IOException {
        //If test fails
        if (result.getStatus() == ITestResult.FAILURE) {
            //take the screenshot and return location of screenshot
            String screenShotPath = BrowserUtils.getScreenshot(result.getMethod().getMethodName());
            //add your screenshot to your report
            extentLogger.fail(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build());
            //capture the exception and put inside the report
            extentLogger.fail(result.getThrowable());
        }
        //close driver
        Driver.closeDriver();
    }

    @AfterTest
    public void tearDownTest() {
        //this is when the report actually created
        extent.flush();
    }
}