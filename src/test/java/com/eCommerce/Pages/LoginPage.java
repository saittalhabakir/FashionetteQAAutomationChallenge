package com.eCommerce.Pages;

import com.eCommerce.Utilities.BrowserUtils;
import com.github.javafaker.Faker;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//span[@class='icon icon--user']")
    public WebElement loginIcon;

    @FindBy(xpath = "//input[@name='email']")
    public WebElement emailField;

    @FindBy(xpath = "//input[@name='password']")
    public WebElement passwordField;

    @FindBy(xpath = "//button[contains(text(),'Login')]")
    public WebElement loginButton;

    @FindBy(xpath = "//div[@class='account__welcome text__center font-size--hero']")
    public WebElement welcomeMessage;

    @FindBy(xpath = "//input[@name='first_name']")
    public WebElement nameField;

    @FindBy(xpath = "//input[@name='last_name']")
    public WebElement lastNameField;

    @FindBy(xpath = "//*[@class='account__content__column account__content__column--customer'][1]")
    public WebElement customerInformation;

    @FindBy(xpath = "//h1[text()='Welcome to fashionette!']")
    public WebElement loginPageWelcomeMessage;

    @FindBy(id = "email-error")
    public WebElement emailErrorMessage;

    /**
     * All the following method uses to login with credentials and customized user information
     * @param username
     * @param password
     */

    public void login(String username, String password) {

        BrowserUtils.waitFor(1);
        emailField.sendKeys(username);
        BrowserUtils.waitForVisibility(passwordField, 15);
        BrowserUtils.waitFor(1);
        passwordField.sendKeys(password);
        loginButton.click();
    }

    Faker faker = new Faker();
    String generateName = faker.name().firstName();
    String generateLastName = faker.name().lastName();

    public void modifyCustomerInformation() {

        BrowserUtils.waitFor(1);
        nameField.clear();
        nameField.sendKeys(generateName);
        BrowserUtils.waitFor(1);
        lastNameField.clear();
        lastNameField.sendKeys(generateLastName);
    }

    public void verifyCustomerInformation() {

        BrowserUtils.waitFor(1);
        String expectedInformation = customerInformation.getText();
        ArrayList<String> firstNameAndLastName = new ArrayList<>(Arrays.asList(expectedInformation.split("28")));
        String actualFirstNameAndLastName = firstNameAndLastName.get(0).toString().trim();
        String expectedFirstNameAndLastName = "Mr "+ generateName + " " + generateLastName;
        assertEquals(expectedFirstNameAndLastName, actualFirstNameAndLastName);
    }
}
