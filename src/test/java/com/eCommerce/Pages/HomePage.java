package com.eCommerce.Pages;

import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(css = "#usercentrics-root")
    public WebElement shadowDomElement;

    /**
     * This methiod uses to accept all cookies when we are navigated in home page
     */

    public void AcceptAllCookies() {
        SearchContext last = (SearchContext) ((JavascriptExecutor) Driver.get()).executeScript("return arguments[0].shadowRoot", shadowDomElement);
        BrowserUtils.waitFor(2);
        last.findElement(By.cssSelector(".sc-gsDKAQ")).click();
    }
}
