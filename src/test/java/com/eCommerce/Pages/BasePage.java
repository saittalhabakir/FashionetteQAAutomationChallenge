package com.eCommerce.Pages;

import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public abstract class BasePage {

    // To use page factory design pattern in POM
    public BasePage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(Driver.get(), 10), this);
    }

    @FindBy(xpath = "//*[@class='header__cart-icon']")
    public WebElement cartIcon;

    @FindBy(xpath = "//input[@placeholder='Coupon']")
    public WebElement couponField;

    @FindBy(xpath = "//td[@class='text__right typography__type-2--bold']")
    public WebElement cartTotalPrice;

    @FindBy(css = "div.account--personaldata > div > div:nth-child(2) > div > div")
    public WebElement editBtn;

    @FindBy(css = "div.account--address__action.account--address__action--save")
    public WebElement saveBtn;

    /**
     * All the following method uses to apply a voucher scenario
     */

    public void visitCart() {
        cartIcon.click();
    }

    public void clickVoucher(String str) {

        WebElement voucherButton = Driver.get().findElement(By.xpath("(//*[contains(text(),'" + str + "')])[1]"));
        BrowserUtils.waitForVisibility(voucherButton, 10);
        voucherButton.click();
    }

    public void applyValidVoucher(String str) {
        couponField.sendKeys(str);
    }

    public void clickRedeemButton(String str){

        WebElement redeemButton = Driver.get().findElement(By.xpath("//button[normalize-space()='"+str+"']"));
        BrowserUtils.waitFor(3);
        redeemButton.click();
    }

    public boolean verifyDiscount(String str){

        WebElement subTotal = Driver.get().findElement(By.xpath("//td[@class='text__right typography__type-2 cart__"+str+"']"));
        String str1 = subTotal.getText();
        BrowserUtils.waitFor(3);
        String str2 = cartTotalPrice.getText();

        double subTotalPrice = Double.parseDouble(str1.substring(1));
        double totalPrice = Double.parseDouble(str2.substring(1));

        if (subTotalPrice>totalPrice){
            return true;
        }else {
            return false;
        }
    }

    public void navigateUserInformationMenus(String str) {
        WebElement personalData = Driver.get().findElement(By.xpath("//a[normalize-space()='"+str+"']"));
        personalData.click();
    }
}


