package com.eCommerce.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CustomerPage extends BasePage{

    @FindBy (name = "first_name")
    public WebElement firstNameField;

    @FindBy (id = "first_name-error")
    public WebElement firstNameError;

    /**
     * All the following method uses to enter user information
     * @param firstName
     */

    public void fillFirstNameField(String firstName){
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
    }

    public String getFirstNameErrorMessage(){
        return firstNameError.getText();
    }
}
