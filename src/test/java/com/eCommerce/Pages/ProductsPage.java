package com.eCommerce.Pages;

import com.eCommerce.Utilities.BrowserUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductsPage extends BasePage {

    @FindBy(xpath = "//div[@class='signpost--wrap'][6]")
    public WebElement selectMainProduct;

    @FindBy(xpath = "(//*[@class='product--list__item__image'])[5]")
    public WebElement selectProduct;

    /**
     * This method uses to select a product
     */

    public void selectProduct() {
        selectMainProduct.click();
        BrowserUtils.waitFor(1);
        selectProduct.click();
    }
}
