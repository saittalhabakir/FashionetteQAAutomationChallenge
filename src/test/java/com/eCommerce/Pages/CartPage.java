package com.eCommerce.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {

    @FindBy(xpath = "(//*[@class='btn__content'])[3]")
    public WebElement addToCartButton;

    @FindBy(xpath = "//div[@value='A0183406_ONE']//span[1]")
    public WebElement selectSize;

    @FindBy (xpath = "//div[@class='cart-item--img']//img")
    public WebElement selectedProductImage;

    @FindBy (xpath = "//a[normalize-space()='redeem']")
    public WebElement reducePrice;

    @FindBy (xpath = "//i[@class='icon icon--inline icon--cross']")
    public WebElement cancelIcon;

    @FindBy (xpath = "//*[contains(text(),'Your voucher')]")
    public WebElement voucherErrorMessage;

    /**
     * All the following method uses to add product to card
     * @return
     */

    public String getVoucherErrorMessage(){
        return voucherErrorMessage.getText();
    }

    public void selectProductSize(){
        selectSize.click();
    }

    public void productAdder() {
        addToCartButton.click();
    }
}
