package com.eCommerce.Tests;

import com.eCommerce.Pages.CartPage;
import com.eCommerce.Pages.HomePage;
import com.eCommerce.Pages.LoginPage;
import com.eCommerce.Pages.ProductsPage;
import com.eCommerce.TestBase;
import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.ConfigurationReader;
import com.eCommerce.Utilities.Driver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TC01_AddProductToCardAndLoginTest extends TestBase {

    @BeforeMethod
    public void setUp() {
        homePage = new HomePage();
        loginPage = new LoginPage();
        cartPage = new CartPage();
        productsPage = new ProductsPage();
    }

    @Test(priority = 1)
    public void negativeLoginFunctionalityTest() {

        extentLogger = extent.createTest("User should successfully see warning message while tries to login");

        loginPage.loginIcon.click();
        BrowserUtils.waitFor(1);
        String actualURL = Driver.get().getCurrentUrl();
        assertTrue(actualURL.contains("login"));
        extentLogger.info("User navigate login page and verify that current url is contains 'login' ");

        String actualWelcomeMessage = loginPage.loginPageWelcomeMessage.getText();
        assertEquals("WELCOME TO FASHIONETTE!", actualWelcomeMessage);
        extentLogger.info("User should be able to see welcome message");

        String username = ConfigurationReader.get("invalidUsername");
        String password = ConfigurationReader.get("invalidPassword");
        BrowserUtils.waitFor(1);
        loginPage.login(username, password);
        extentLogger.info("User should be able to login with invalid credentials");

        String actualEmailErrorMessage = loginPage.emailErrorMessage.getText();
        assertEquals(actualEmailErrorMessage,"Please enter your e-mail in the correct format. This address must include an '@' symbol and a full stop'.'.");

        extentLogger.pass("Test is pass");
    }

    @Test(priority = 2)
    public void addProductToCart() {

        extentLogger = extent.createTest("User should successfully add product to the card and login");

        productsPage.selectProduct();
        extentLogger.info("User should be able to select a product");
        cartPage.selectProductSize();
        extentLogger.info("User should be able to selects product details");
        cartPage.productAdder();
        extentLogger.info("User adds a product to the cart");

        loginPage.loginIcon.click();
        BrowserUtils.waitFor(1);
        String actualURL = Driver.get().getCurrentUrl();
        assertTrue(actualURL.contains("login"));
        extentLogger.info("User navigate login page and verify that current url is contains 'login' ");

        String actualWelcomeMessage = loginPage.loginPageWelcomeMessage.getText();
        assertEquals("WELCOME TO FASHIONETTE!", actualWelcomeMessage);
        extentLogger.info("User should be able to see welcome message");

        String username = ConfigurationReader.get("username");
        String password = ConfigurationReader.get("password");
        BrowserUtils.waitFor(2);
        loginPage.login(username, password);
        extentLogger.info("User should be able to login with valid credentials");

        String actualWelcomeToYourAccountMessage = loginPage.welcomeMessage.getText();
        assertTrue(actualWelcomeToYourAccountMessage.contains("welcome to your account."));
        extentLogger.info("Verify that welcome to your account message is successfully displayed");

        cartPage.visitCart();
        extentLogger.info("User visits the cart");
        BrowserUtils.waitFor(2);
        String actualVisitCartUrl = Driver.get().getCurrentUrl();
        assertTrue(actualVisitCartUrl.contains("cart"));
        extentLogger.info("verify that current url contains cart");
        assertTrue(cartPage.selectedProductImage.isDisplayed());
        extentLogger.info("verify that selected product appeared in the user cart");

        extentLogger.pass("Test is pass");
    }
}
