package com.eCommerce.Tests;

import com.eCommerce.Pages.*;
import com.eCommerce.TestBase;
import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.ConfigurationReader;
import com.eCommerce.Utilities.Driver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TC02_ModifyUserInformationTest extends TestBase {

    @BeforeMethod
    public void setUp() {
        loginPage = new LoginPage();
        customerPage = new CustomerPage();
    }

    @Test(priority = 1)
    public void negativeModifyUserInformationTest() {

        extentLogger = extent.createTest("User should not modifies user information");

        loginPage.loginIcon.click();
        BrowserUtils.waitFor(1);
        String actualURL = Driver.get().getCurrentUrl();
        assertTrue(actualURL.contains("login"));
        extentLogger.info("User navigate login page and verify that current url is contains 'login' ");

        String actualWelcomeMessage = loginPage.loginPageWelcomeMessage.getText();
        assertEquals("WELCOME TO FASHIONETTE!", actualWelcomeMessage);
        extentLogger.info("User should be able to see welcome message");

        String username = ConfigurationReader.get("username");
        String password = ConfigurationReader.get("password");
        BrowserUtils.waitFor(2);
        loginPage.login(username, password);
        extentLogger.info("User should be able to login with valid credentials");

        String actualWelcomeToYourAccountMessage = loginPage.welcomeMessage.getText();
        assertTrue(actualWelcomeToYourAccountMessage.contains("welcome to your account."));
        extentLogger.info("Verify that welcome to your account message is successfully displayed");

        loginPage.navigateUserInformationMenus("Personal data");
        extentLogger.info("User clicks on Personal data menu");
        loginPage.editBtn.click();
        extentLogger.info("User clicks on Edit button");

        customerPage.fillFirstNameField("abcdefghijklmnoprstuüvyzwABCDEFGHIJKLMNOPRSTUÜVYZW");
        String expectedFirstNameMessage = "The \"first name\" may not be greater than 20 characters.";
        assertEquals(expectedFirstNameMessage, customerPage.getFirstNameErrorMessage());
        extentLogger.info("Verify that the first name may not be greater than twenty characters");

        extentLogger.pass("Test is pass");
    }

    @Test(priority = 2)
    public void modifyUserInformationTest() {

        extentLogger = extent.createTest("User should successfully modifies user information");

        loginPage.loginIcon.click();
        BrowserUtils.waitFor(1);
        String actualURL = Driver.get().getCurrentUrl();
        assertTrue(actualURL.contains("login"));
        extentLogger.info("User navigate login page and verify that current url is contains 'login' ");

        String actualWelcomeMessage = loginPage.loginPageWelcomeMessage.getText();
        assertEquals("WELCOME TO FASHIONETTE!", actualWelcomeMessage);
        extentLogger.info("User should be able to see welcome message");

        String username = ConfigurationReader.get("username");
        String password = ConfigurationReader.get("password");
        BrowserUtils.waitFor(2);
        loginPage.login(username, password);
        extentLogger.info("User should be able to login with valid credentials");

        String actualWelcomeToYourAccountMessage = loginPage.welcomeMessage.getText();
        assertTrue(actualWelcomeToYourAccountMessage.contains("welcome to your account."));
        extentLogger.info("Verify that welcome to your account message is successfully displayed");

        loginPage.navigateUserInformationMenus("Personal data");
        extentLogger.info("User clicks on Personal data menu");
        loginPage.editBtn.click();
        extentLogger.info("User clicks on Edit button");
        loginPage.modifyCustomerInformation();
        extentLogger.info("User successfully modify the name and the surname of the user");
        loginPage.saveBtn.click();
        extentLogger.info("User clicks on Save button");
        loginPage.verifyCustomerInformation();
        extentLogger.info("Verify that user information has been successfully saved");

        extentLogger.pass("Test is pass");
    }
}
