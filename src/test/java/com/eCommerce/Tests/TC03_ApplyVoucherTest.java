package com.eCommerce.Tests;

import com.eCommerce.Pages.CartPage;
import com.eCommerce.Pages.CustomerPage;
import com.eCommerce.Pages.LoginPage;
import com.eCommerce.Pages.ProductsPage;
import com.eCommerce.TestBase;
import com.eCommerce.Utilities.BrowserUtils;
import com.eCommerce.Utilities.Driver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TC03_ApplyVoucherTest extends TestBase {

    @BeforeMethod
    public void setUp() {
        productsPage = new ProductsPage();
        cartPage = new CartPage();
    }

    @Test(priority = 1)
    public void negativeApplyVoucherTest() {

        extentLogger = extent.createTest("User should not apply a voucher");

        productsPage.selectProduct();
        extentLogger.info("User should be able to select a product");
        cartPage.selectProductSize();
        extentLogger.info("User should be able to selects product details");
        cartPage.productAdder();
        extentLogger.info("User adds a product to the cart");

        cartPage.visitCart();
        extentLogger.info("User visits the cart");
        BrowserUtils.waitFor(1);
        String actualVisitCartUrl = Driver.get().getCurrentUrl();
        assertTrue(actualVisitCartUrl.contains("cart"));
        extentLogger.info("Verify that current url contains cart");
        assertTrue(cartPage.selectedProductImage.isDisplayed());
        extentLogger.info("Verify that selected product appeared in the user cart");

        cartPage.clickVoucher("Voucher");
        extentLogger.info("User clicks on Voucher link");
        cartPage.applyValidVoucher("Wrong Coupon!");
        extentLogger.info("User applies a voucher to the coupon field");
        cartPage.clickRedeemButton("redeem");
        extentLogger.info("User clicks on redeem button");
        assertEquals("Your voucher \"Wrong Coupon!\" is not valid", cartPage.getVoucherErrorMessage());
        extentLogger.info("Verify that the user should be able to see invalid voucher message");
        assertFalse(cartPage.verifyDiscount("subtotal"));
        extentLogger.info("Verify that subtotal is not greater than total amount");

        extentLogger.pass("Test is pass");
    }

    @Test(priority = 2)
    public void applyVoucherTest() {

        extentLogger = extent.createTest("User should successfully apply a voucher");

        productsPage.selectProduct();
        extentLogger.info("User should be able to select a product");
        cartPage.selectProductSize();
        extentLogger.info("User should be able to selects product details");
        cartPage.productAdder();
        extentLogger.info("User adds a product to the cart");

        cartPage.visitCart();
        extentLogger.info("User visits the cart");
        BrowserUtils.waitFor(1);
        String actualVisitCartUrl = Driver.get().getCurrentUrl();
        assertTrue(actualVisitCartUrl.contains("cart"));
        extentLogger.info("Verify that current url contains cart");
        assertTrue(cartPage.selectedProductImage.isDisplayed());
        extentLogger.info("Verify that selected product appeared in the user cart");

        cartPage.clickVoucher("Voucher");
        extentLogger.info("User clicks on Voucher link");
        cartPage.applyValidVoucher("qachallenge");
        extentLogger.info("User applies a voucher to the coupon field");
        cartPage.clickRedeemButton("redeem");
        extentLogger.info("User clicks on redeem button");
        assertTrue(cartPage.cancelIcon.isEnabled());
        assertTrue(cartPage.reducePrice.isEnabled());
        extentLogger.info("Verify that the valid voucher is applied properly");
        assertTrue(cartPage.verifyDiscount("subtotal"));
        extentLogger.info("Verify that subtotal is greater than total amount");

        extentLogger.pass("Test is pass");
    }
}
